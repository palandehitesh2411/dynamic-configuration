const DEFAULT = null;
const SERVICE_NAME = 'dynamic-configuration'; 

module.exports = {
  DEFAULT,
  SERVICE_NAME
};