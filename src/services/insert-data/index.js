
const {knex : connect} = require('./../../../config/db');
const safePromise = require('./../../utilities/safe-promise');

async function insertData(payload){
 
  const { name, ...rest} = payload;
  
  const [err, data] = await safePromise(connect('config').insert({name}));
  if(err){
    throw err;
  }
  const objKeys = Object.keys(rest);
  if(objKeys > 0){
    const tempArr = [];
    objKeys.forEach(item =>{
      const row = {};
      row["user_id"] = data[0];
      row["column_key"] = item;
      row["column_value"] = rest[item];
      tempArr.push(row);
    });
  
    const [error] = await safePromise(connect('config_info').insert(tempArr));
    if(error){
      throw error;
    }
  }

  return data;
}

module.exports = insertData;