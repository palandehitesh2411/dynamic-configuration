
const { knex:connect } = require('./../../../config/db');
const safePromise = require('./../../utilities/safe-promise');

async function getData(id){
  // eslint-disable-next-line no-unused-vars
  const [error,hold]= await safePromise(connect('config_info')
    .select('config_info.user_id')
    .where('config_info.user_id',id));
 
  if(hold.length){
    return connect('config')
      .join('config_info','config.id','config_info.user_id')
      .select('config.name','config_info.column_key','config_info.column_value')
      .where('config.id', id) 
  }
  else{
    return connect('config')
      .select('config.name')
      .where('config.id', id)
  }   
}
module.exports = getData;
