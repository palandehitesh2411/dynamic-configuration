'use strict';

const welcome = require('./welcome');
const insertData = require('./insert-data');
const getData = require('./get-data');
module.exports = {
  welcome: welcome,
  insertData : insertData,
  getData : getData
}