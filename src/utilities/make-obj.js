function makeObj(data){
  const userRes= {};
  userRes.name = data[0].name;
  data.forEach(element => {

    userRes[element.column_key] = element.column_value;
  });
  return userRes;
}

module.exports = makeObj;