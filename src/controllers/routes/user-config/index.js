
const express = require('express');
const router = express.Router();
const middlewares = require('../../../middlewares');
const rules = require('../../../../rules');
const { safePromise, makeObj } = require('../../../utilities');
const insertData = require('../../../services/insert-data');
const getData = require('../../../services/get-data');

const validate = middlewares.validate;
const sanitize = middlewares.sanitize;


router
  .route('/post-config')
  .post(validate(rules.dbSetData), async function(req, res){
    const  body = req.body;
  
    const[error, data] = await safePromise(insertData(body));
    if(error){
      return res.status(500).json({
        message : "Error occured while inserting data "
      });
    }
    res.json({
      message : "Data Inserted successfully",
      res : {
        data : `Your id : ${data}`
      }
    })
  });



router
  .route('/get-config-data')
  .get(sanitize, validate(rules.dbGetData), async function(req, res){
    const id = req.payload.id;
    const [err, data] = await safePromise(getData(id));
    if(err){
      return res.json({
        message: `Error occured while reading the data ${err}`,
        res:{
          err
        }
      });
    }else{
      const userRes = makeObj(data);
      res.json({
        message: "Fetched Successfully!!",
        res: {
          userRes
        }
      });
    }
    

  }) 

module.exports = router;


